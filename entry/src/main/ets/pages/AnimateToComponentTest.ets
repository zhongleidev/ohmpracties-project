@Entry
@Component
struct AnimateToComponentTest {

  @State translate_test: TranslateOptions = { // 位移数据
    x: 0,
    y: 0,
    z: 0
  }

  private step1: () => Promise<boolean>; // 第一步动画
  private step2: () => Promise<boolean>; // 第二步动画
  private step3: () => Promise<boolean>; // 第三步动画
  private step4: () => Promise<boolean>; // 第四步动画

  build() {
    Column({space: 10}) {
      Button('animateTo')
        .onClick(async () => {
          await this.step1();            // 等待第一步动画执行完毕
          await this.step2();            // 等待第二步动画执行完毕
          await this.step3();            // 等待第三步动画执行完毕
          await this.step4();            // 等待第四步动画执行完毕
        })

      Text("AnimateTo")
        .fontSize(20)
        .backgroundColor("#aabbcc")
        .translate(this.translate_test)
    }
    .width('100%')
    .height('100%')
    .padding(10)
  }

  private animateStep(value: AnimateParam, event: () => void): () => Promise<boolean> {
    return () => {
      return new Promise((resolve, reject) => {
        if(value) {                      // 判断参数是否合法
          let onFinish = value.onFinish; // 保存原有动画回调
          value.onFinish = () => {       // 替换新的动画回调
            onFinish?.call(this)         // 执行原有动画回到
            resolve(true);               // 触发方法执行完毕
          }
          animateTo(value, event);       // 开始执行显式动画
        } else {
          reject("value invalid")     // 触发方法执行失败
          // resolve(false);                // 参数非法，不执行
        }
      });
    }
  }

  aboutToAppear() {
    let duration = 300;
    this.step1 = this.animateStep({      // 初始化单步动画1
      duration: duration,
      tempo: 0.5,
      curve: Curve.Linear,
      iterations: 1,
      playMode: PlayMode.Normal,
      onFinish: () => {
        console.log("animation finish")
      }
    }, () => {
      this.translate_test = {
        x: 0,
        y: 100,
        z: 0
      }
    });

    this.step2 = this.animateStep({      // 初始化单步动画2
      duration: duration,
      tempo: 0.5,
      curve: Curve.Linear,
      iterations: 1,
      playMode: PlayMode.Normal,
      onFinish: () => {
        console.log("animation finish")
      }
    }, () => {
      this.translate_test = {
        x: 100,
        y: 100,
        z: 0
      }
    });

    this.step3 = this.animateStep({      // 初始化单步动画3
      duration: duration,
      tempo: 0.5,
      curve: Curve.Linear,
      iterations: 1,
      playMode: PlayMode.Normal,
      onFinish: () => {
        console.log("animation finish")
      }
    }, () => {
      this.translate_test = {
        x: 100,
        y: 0,
        z: 0
      }
    });

    this.step4 = this.animateStep({      // 初始化单步动画4
      duration: duration,
      tempo: 0.5,
      curve: Curve.Linear,
      iterations: 1,
      playMode: PlayMode.Normal,
      onFinish: () => {
        console.log("animation finish")
      }
    }, () => {
      this.translate_test = {
        x: 0,
        y: 0,
        z: 0
      }
    });
  }

  // 提取一个方法
  generateAnimateParam(duration: number, event: () => void): AnimateParam {
    return {
      duration: duration,
      tempo: 0.5,
      curve: Curve.Linear,
      iterations: 1,
      playMode: PlayMode.Normal,
      onFinish: () => {
        event?.call(this);
      }
    }
  }
}