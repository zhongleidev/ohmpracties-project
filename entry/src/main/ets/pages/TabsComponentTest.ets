@Entry
@Component
struct TabsComponentTest {
  private controller: TabsController = new TabsController();

  @State index: number = 0; // 选项卡下标，默认为第一个

  @Builder tabMessage() {   // 自定义消息标签
    Column() {
      Column() {
        Blank()
        Image(this.index == 0 ? 'icons/icon_message_selected.png' : 'icons/icon_message_normal.png')
          .size({width: 25, height: 25})
        Text('消息')
          .fontSize(16)
          .fontColor(this.index == 0 ? "#2a58d0" : "#6b6b6b")
        Blank()
      }
      .height('100%')
      .width("100%")
      .onClick(() => {
        this.index = 0;
        this.controller.changeIndex(this.index);
      })
    }
  }

  @Builder tabContract() {  // 自定义联系人标签
    Column() {
      Blank()
      Image(this.index == 1 ? 'icons/icon_contract_selected.png' : 'icons/icon_contract_normal.png')
        .size({width: 25, height: 25})
      Text('联系人')
        .fontSize(16)
        .fontColor(this.index == 1 ? "#2a58d0" : "#6b6b6b")
      Blank()
    }
    .height('100%')
    .width("100%")
    .onClick(() => {
      this.index = 1;
      this.controller.changeIndex(this.index);
    })
  }

  @Builder tabDynamic() {   // 自定义动态标签
    Column() {
      Blank()
      Image(this.index == 2 ? 'icons/icon_dynamic_selected.png' : 'icons/icon_dynamic_normal.png')
        .size({width: 25, height: 25})
      Text('动态')
        .fontSize(16)
        .fontColor(this.index == 2 ? "#2a58d0" : "#6b6b6b")
      Blank()
    }
    .height('100%')
    .width("100%")
    .onClick(() => {
      this.index = 2;
      this.controller.changeIndex(this.index);
    })
  }

  build() {
    Column() {
      Tabs({
        barPosition: BarPosition.End, // TabBar排列在下方
        controller: this.controller   // 绑定控制器
      }) {
        TabContent() {
          Column() {
            Text('消息')
              .fontSize(30)
          }
          .width('100%')
          .height('100%')
          .backgroundColor("#aabbcc")
        }
        .tabBar(this.tabMessage)      // 使用自定义TabBar

        TabContent() {
          Column() {
            Text('联系人')
              .fontSize(30)
          }
          .width('100%')
          .height('100%')
          .backgroundColor("#bbccaa")
        }
        .tabBar(this.tabContract)     // 使用自定义TabBar

        TabContent() {
          Column() {
            Text('动态')
              .fontSize(30)
          }
          .width('100%')
          .height('100%')
          .backgroundColor("#ccaabb")
        }
        .tabBar(this.tabDynamic)      // 使用自定义TabBar
      }
      .width('100%')
      .height('100%')
      .barHeight(60)
      .barMode(BarMode.Fixed)         // TabBar均分
      .onChange((index: number) => {  // 页面切换回调
        this.index = index;
      })
    }
    .width('100%')
    .height('100%')
  }
}